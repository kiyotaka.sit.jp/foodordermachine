import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

public class FoodOrderMachine {
    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderMachine");
        frame.setContentPane(new FoodOrderMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    int total=0;
    int pizza=0;
    void order(String food, int price){int confirmation = JOptionPane.showConfirmDialog(
            null,
            "Would you like to order "+food+"?",
            "Order Confirmation",
            JOptionPane.YES_NO_OPTION);
        if(confirmation==0){
            String currentText = OrderedItemsList.getText();
            OrderedItemsList.setText(currentText + food+"\n");
            JOptionPane.showMessageDialog(null, "Order for "+food+" received.");
            total+=price;
            PriceLabel.setText("Total Price ¥"+total);
            pizza++;
        }
    }
    private JPanel root;
    private JLabel topLabel;
    private JButton margeritaButton;
    private JButton bazilPizzaButton;
    private JButton chikenAndPotatoPizzaButton;
    private JButton mayoAndCornPizzaButton;
    private JButton sixKindsOfCheeseButton;
    private JButton pepperoniPizzaButton;
    private JTextPane OrderedItemsList;
    private JButton checkOutButton;
    private JLabel OrderList;
    private JLabel PriceLabel;
    private JCheckBox Storepickup;
    private JFormattedTextField Pickup;

    public FoodOrderMachine() {
        margeritaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Margerita", 1800);
            }
        });
        margeritaButton.setIcon(new ImageIcon(getClass().getResource("Food Image/Margerita.jpeg")));
        margeritaButton.setText("Margerita ¥1800");
        margeritaButton.setHorizontalTextPosition(JLabel.CENTER);
        margeritaButton.setVerticalTextPosition(JLabel.BOTTOM);

        bazilPizzaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Bazil Pizza", 1700);
            }
        });
        bazilPizzaButton.setIcon(new ImageIcon(getClass().getResource("Food Image/Bazil pizza.jpeg")));
        bazilPizzaButton.setText("Bazil Pizza ¥1700");
        bazilPizzaButton.setHorizontalTextPosition(JLabel.CENTER);
        bazilPizzaButton.setVerticalTextPosition(JLabel.BOTTOM);

        chikenAndPotatoPizzaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Chiken and Potato Pizza", 2000);
            }
        });
        chikenAndPotatoPizzaButton.setIcon(new ImageIcon(getClass().getResource("Food Image/Chiken potato pizza.jpeg")));
        chikenAndPotatoPizzaButton.setText("Chiken and Potato Pizza ¥2000");
        chikenAndPotatoPizzaButton.setHorizontalTextPosition(JLabel.CENTER);
        chikenAndPotatoPizzaButton.setVerticalTextPosition(JLabel.BOTTOM);

        mayoAndCornPizzaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Mayo and Cone Pizza", 1500);
            }
        });
        mayoAndCornPizzaButton.setIcon(new ImageIcon(getClass().getResource("Food Image/Mayo corn pizza.jpeg")));
        mayoAndCornPizzaButton.setText("Mayo and Cone Pizza ¥1500");
        mayoAndCornPizzaButton.setHorizontalTextPosition(JLabel.CENTER);
        mayoAndCornPizzaButton.setVerticalTextPosition(JLabel.BOTTOM);

        sixKindsOfCheeseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Six kinds of cheese Pizza", 1900);
            }
        });
        sixKindsOfCheeseButton.setIcon(new ImageIcon(getClass().getResource("Food Image/Six cheese variety pizza.jpeg")));
        sixKindsOfCheeseButton.setText("Six kinds of cheese Pizza ¥1900");
        sixKindsOfCheeseButton.setHorizontalTextPosition(JLabel.CENTER);
        sixKindsOfCheeseButton.setVerticalTextPosition(JLabel.BOTTOM);

        pepperoniPizzaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Pepperoni Pizza", 2900);
            }
        });
        pepperoniPizzaButton.setIcon(new ImageIcon(getClass().getResource("Food Image/Pepperoni pizza.jpeg")));
        pepperoniPizzaButton.setText("Pepperoni Pizza ¥2900");
        pepperoniPizzaButton.setHorizontalTextPosition(JLabel.CENTER);
        pepperoniPizzaButton.setVerticalTextPosition(JLabel.BOTTOM);

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(total==0){
                    JOptionPane.showMessageDialog(
                            null,
                            "Nothing is ordered."
                    );
                }
                else {
                    int confirmation = JOptionPane.showConfirmDialog(
                            null,
                            "Would you like to checkout?",
                            "Checkout Confirmation",
                            JOptionPane.YES_NO_OPTION);
                    if (confirmation == 0) {
                        if(Storepickup.isSelected()){
                            if(pizza>=2){
                                total-=1000;
                            }
                        }
                        JOptionPane.showMessageDialog(
                                null,
                                "Thank you. The total price is " + total + " yen."
                        );
                        OrderedItemsList.setText(null);
                        total = 0;
                    }
                }
            }
        });

        PriceLabel.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                PriceLabel.setText("Total Price ¥"+total);
            }
        });

    }
}
